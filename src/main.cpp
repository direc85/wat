#include <QtCore/QCoreApplication>
#include <QtCore/QCommandLineParser>

#include "qstdout.h"
#include "database.h"

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);
    app.setApplicationName("wat");
    app.setApplicationVersion("0.0.1");

    QCommandLineParser parser;
    parser.setApplicationDescription(
        "A simple sticky notes database thingy.\n\n"
        "Quickstart:\n"
        "  wat add focus Ride!  Create a new item\n"
        "  wat focus name Car!  Update the name of an item\n"
        "  wat focus year 2002  Set the 'year' property of item 'focus' to '2002'\n"
        "  wat focus radio Sony Set the 'radio' property of item 'focus' to 'Sony'\n"
        "  wat focus year       Print the 'year' property of item 'focus'\n"
        "  wat focus            Print all properties of item 'focus'\n"
        "  wat focus del year   Delete property 'year' of item 'focus'\n"
        "  wat del focus        Delete item 'focus' with all its properties\n\n"
        "Note: 'add', 'del' and 'name' can't be used as item or property names."
    );
    parser.addHelpOption();
    parser.addPositionalArgument("name", "Name of the item", "[name]");
    parser.addPositionalArgument("prop", "Property name of an item", "[prop]");
    parser.addPositionalArgument("value", "Value for the property", "[value]");

    parser.addOption(QCommandLineOption("debug", "Print SQL queries and errors"));
    parser.addOption(QCommandLineOption("items", "List all items"));
    parser.addOption(QCommandLineOption("props", "List all property names"));
    parser.addOption(QCommandLineOption("version", "Print application version"));
    parser.addOption(QCommandLineOption("n", "Search from item and names", "name"));
    parser.addOption(QCommandLineOption("p", "List all items with such property", "prop"));
    parser.addOption(QCommandLineOption("v", "Search from property values", "value"));
    parser.process(app);

    // If not arguments were given, print help text
    if(app.arguments().length() <= 1) {
        qStdOut() << parser.helpText();
        return 0;
    }

    // We can also handle the version option before database initialization
    if (parser.isSet("version")) {
        qStdOut() << app.applicationName() << " " << app.applicationVersion() << Qt::endl;
        return 0;
    }

    QStringList posArgs = parser.positionalArguments();

    int retval = 0;
    auto db = new Database(parser.isSet("debug"), nullptr);
    if (db->prepareDatabase()) {
        if (parser.isSet("items")) {
            db->showAllItems();
        } else if (parser.isSet("props")) {
            db->showAllProperties();
        } else if (parser.isSet("n")) {
            db->searchItemsByName(parser.value("n"));
        } else if (parser.isSet("p")) {
            db->searchItemsWithProperties(parser.value("p"));
        } else if (parser.isSet("v")) {
            db->searchItemsByPropertyValue(parser.value("v"));
        } else if (posArgs.length() == 1) {
            db->showItemProperties(posArgs.at(0));
        } else if (posArgs.length() == 2) {
            if(posArgs.at(0) == "del") {
                db->deleteItem(posArgs.at(1));
            } else {
                db->showItemProperty(posArgs.at(0), posArgs.at(1));
            }
        } else if (posArgs.length() == 3) {
            if(posArgs.at(0) == "add") {
                db->addItem(posArgs.at(1), posArgs.at(2));
            } else if (posArgs.at(1) == "name") {
                db->setItemName(posArgs.at(0), posArgs.at(2));
            } else if (posArgs.at(1) == "del") {
                db->deleteItemProperty(posArgs.at(0), posArgs.at(2));
            } else {
                db->setItemProperty(posArgs.at(0), posArgs.at(1), posArgs.at(2));
            }
        } else {
            qStdErr() << "Invalid arguments!";
            retval = 1;
        }
    } else {
        retval = 1;
    }

    delete db;
    return retval;
}
