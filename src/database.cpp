#include "database.h"

Database::Database(bool debug, QObject *parent = nullptr) {
    Q_UNUSED(parent)
    m_debug = debug;
    m_db = new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE"));
    QString storage_path = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QDir appdata(storage_path);
    if(!appdata.exists()) {
        appdata.mkpath(storage_path);
    }
    QString filename = QString("%1%2").arg(storage_path).arg("/wat.sqlite");
    m_db->setDatabaseName(filename);
    m_db->open();
}

Database::~Database() {
    if(m_db) {
        m_db->close();
        delete m_db;
    }
}

bool Database::prepareDatabase() {
    if(!(m_db && m_db->isOpen())) {
        return false;
    }
    if(m_db->tables().length() == 0) {
        QSqlQuery query(*m_db);
        foreach(QString sql, SQL_NEW_DATABASE_QUERIES){
            query.exec(sql);
        }
        qStdErr() << "Database created!" << Qt::endl;
        return true;
    } else {
        QSqlQuery query(*m_db);
        query.exec(SQL_CHECK_TABLES);
        maybePrintError(query);
        if(!query.next()) {
            return false;
        }
        if(query.value(0).toInt() != m_num_tables) {
            qStdErr() << "Unrecognized database!" << Qt::endl;
            return false;
        }
        return true;
    }
}

void Database::showAllItems() {
    QSqlQuery query(*m_db);
    query.exec(SQL_GET_ALL_ITEMS);
    maybePrintError(query);
    while(query.next()) {
        qStdOut()
            << QString("%1\t%2")
                .arg(query.value(0).toString())
                .arg(query.value(1).toString())
            << Qt::endl;
    }
}

void Database::showAllProperties() {
    QSqlQuery query(*m_db);
    query.exec(SQL_GET_ALL_PROPERTIES);
    maybePrintError(query);

    while(query.next()) {
        qStdOut() << query.value(0).toString() << Qt::endl;
    }
}

void Database::searchItemsByName(QString name) {
    QSqlQuery query(*m_db);
    name = QString("\%%1\%").arg(name);
    query.prepare(SQL_SEARCH_ITEMS_BY_NAME);
    query.bindValue(":search_a", name);
    query.bindValue(":search_b", name);
    query.exec();
    maybePrintError(query);
    while(query.next()) {
        qStdOut()
            << query.value(0).toString() << ":\t"
            << query.value(1).toString() << Qt::endl;
    }
}

void Database::searchItemsByPropertyValue(QString value) {
    QSqlQuery query(*m_db);
    query.prepare(SQL_SEARCH_PROPERTIES_BY_VALUE);
    query.bindValue(":value", QString("\%%1\%").arg(value));
    query.exec();
    maybePrintError(query);
    while(query.next()) {
        qStdOut()
            << query.value(0).toString() << ":\t"
            << query.value(1).toString() << Qt::endl;
    }
}

void Database::setItemName(QString short_name, QString long_name) {
    if(!column_regex.exactMatch(short_name)) {
        qStdErr() << "Invalid item: " << short_name << Qt::endl;
        return;
    }
    QSqlQuery query(*m_db);
    query.prepare(SQL_SET_ITEM_NAME);
    query.bindValue(":short_name", short_name);
    query.bindValue(":long_name", long_name);
    maybePrintError(query);
    query.exec();
}

void Database::addItem(QString short_name, QString long_name) {
    if(!column_regex.exactMatch(short_name)) {
        qStdErr() << "Invalid item: " << short_name << Qt::endl;
        return;
    }
    QSqlQuery query(*m_db);
    query.prepare(SQL_ADD_ITEM);
    query.bindValue(":short_name", short_name);
    query.bindValue(":long_name", long_name);
    query.exec();
    maybePrintError(query);
    if(query.numRowsAffected() == 0) {
        qStdErr() << "Item already exists!" << Qt::endl;
    }
}

void Database::deleteItem(QString short_name) {
    if(!column_regex.exactMatch(short_name)) {
        qStdErr() << "Invalid item: " << short_name << Qt::endl;
        return;
    }
    QSqlQuery query(*m_db);
    query.prepare(SQL_DELETE_ITEM_PROPERTIES);
    query.bindValue(":short_name", short_name);
    query.exec();
    maybePrintError(query);

    query.finish();

    query.prepare(SQL_DELETE_ITEM);
    query.bindValue(":short_name", short_name);
    query.exec();
    maybePrintError(query);
}

void Database::deleteItemProperty(QString short_name, QString property) {
    if(!column_regex.exactMatch(short_name)) {
        qStdErr() << "Invalid item: " << short_name << Qt::endl;
        return;
    }
    if(!column_regex.exactMatch(property)) {
        qStdErr() << "Invalid property: " << property << Qt::endl;
        return;
    }
    QSqlQuery query(*m_db);
    query.prepare(SQL_DELETE_ITEM_PROPERTY);
    query.bindValue(":short_name", short_name);
    query.bindValue(":property", property);
    query.exec();
    maybePrintError(query);
}

void Database::showItemProperties(QString short_name) {
    if(!column_regex.exactMatch(short_name)) {
        qStdErr() << "Invalid item: " << short_name << Qt::endl;
        return;
    }

    QSqlQuery query(*m_db);
    query.prepare(SQL_GET_ITEM_PROPERTIES);
    query.bindValue(":short_name", short_name);
    query.exec();
    maybePrintError(query);

    while(query.next()) {
        qStdOut()
            << query.value(0).toString() << ":\t"
            << query.value(1).toString() << Qt::endl;
    }
}

void Database::showItemProperty(QString short_name, QString property) {
    if(!column_regex.exactMatch(short_name)) {
        qStdErr() << "Invalid item: " << short_name << Qt::endl;
        return;
    }

    if(!column_regex.exactMatch(property)) {
        qStdErr() << "Invalid property: " << property << Qt::endl;
        return;
    }

    QSqlQuery query(*m_db);
    query.prepare(SQL_GET_ITEM_PROPERTY);
    query.bindValue(":short_name", short_name);
    query.bindValue(":property", property);
    query.exec();
    maybePrintError(query);

    while(query.next()) {
        qStdOut() << query.value(0).toString() << Qt::endl;
    }
}

void Database::searchItemsWithProperties(QString property) {
    QSqlQuery query(*m_db);
    query.prepare(SQL_SEARCH_PROPERTIES_BY_NAME);
    query.bindValue(":property", QString("\%%1\%").arg(property));
    query.exec();
    maybePrintError(query);

    while(query.next()) {
        qStdOut()
            << query.value(0).toString() << ":\t"
            << query.value(1).toString() << Qt::endl;
    }
}

void Database::setItemProperty(QString short_name, QString property, QString value) {
    if(!column_regex.exactMatch(short_name)) {
        qStdErr() << "Invalid item: " << short_name << Qt::endl;
        return;
    }

    if(!column_regex.exactMatch(property)) {
        qStdErr() << "Invalid property: " << property << Qt::endl;
        return;
    }
    QSqlQuery query(*m_db);
    query.prepare(SQL_GET_ITEM_PROPERTY);
    query.bindValue(":short_name", short_name);
    query.bindValue(":property", property);
    query.exec();
    maybePrintError(query);

    if(query.next()) {
        query.finish();
        query.prepare(SQL_UPDATE_ITEM_PROPERTY);
        query.bindValue(":value", value);
        query.bindValue(":short_name", short_name);
        query.bindValue(":property", property);
        query.exec();
        maybePrintError(query);
    } else {
        query.finish();
        query.prepare(SQL_ADD_ITEM_PROPERTY);
        query.bindValue(":short_name", short_name);
        query.bindValue(":property", property);
        query.bindValue(":value", value);
        query.exec();
        maybePrintError(query);
    }
}

void Database::createRows(QList<QStringList> &rows, int columns, int lengths[], QSqlQuery &query) {
    QSqlRecord localRecord = query.record();
    QStringList row;
    for (int i = 0; i < localRecord.count(); ++i) {
        row << localRecord.fieldName(i).replace(QString("_"), QString(" ")).trimmed();
        lengths[i] = qMax(row.last().length(), lengths[i]);
    }
    rows << row;
    row.clear();
        for (int i = 0; i < localRecord.count(); ++i) {
        row << "";
    }
    rows << row;

    while(query.next()) {
        QStringList row;

        for (int i = 0; i < columns; ++i) {
            const QString text = query.value(i).toString();
            lengths[i] = qMax(text.length(), lengths[i]);
            row << text;
        }

        rows << row;
    }
}

void Database::maybePrintError(QSqlQuery& query) {
    if(!m_debug) {
        return;
    }

    if(query.lastError().isValid()) {
        qStdErr() << "[DATABASE]  " << query.lastError().databaseText() << Qt::endl;
        qStdErr() << "[DRIVER] " << query.lastError().driverText() << Qt::endl;
    }
    if(query.lastQuery().length() > 0) {
        qStdErr() << "[QUERY] " << query.lastQuery() << Qt::endl;
    }
}
