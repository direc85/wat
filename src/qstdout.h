#ifndef __QSTDOUT_H__
#define __QSTDOUT_H__

#include <QtCore/QTextStream>

QTextStream& qStdOut();

QTextStream& qStdErr();

#endif