#ifndef __DATABASE_H__
#define __DATABASE_H__

#include <QtCore/QObject>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QtSql/QSqlRecord>
#include <QtCore/QTextStream>
#include <QtCore/QFile>
#include <QtCore/QDir>
#include <QtCore/QStandardPaths>
#include "qstdout.h"

class Database: public QObject
{
    Q_OBJECT
public:
    Database(bool debug, QObject *parent);
    ~Database();

    bool prepareDatabase();

    // Items
    void addItem(QString short_name, QString long_name);
    void deleteItem(QString short_name);
    void showAllItems();
    void setItemName(QString short_name, QString long_name);

    // Properties
    void deleteItemProperty(QString short_name, QString property);
    void showItemProperties(QString short_name);
    void showItemProperty(QString short_name, QString property);
    void showAllProperties();
    void setItemProperty(QString short_name, QString property, QString value);

    // Searches
    void searchItemsWithProperties(QString property);
    void searchItemsByName(QString name);
    void searchItemsByPropertyValue(QString value);

private:
    QSqlDatabase* m_db;
    bool m_debug = false;
    void createRows(QList<QStringList> &rows, int columns, int lengths[], QSqlQuery &query);
    void maybePrintError(QSqlQuery &query);

    QRegExp column_regex = QRegExp("[a-z][a-z0-9_]*", Qt::CaseSensitive);

    const int m_num_tables = 2;
    const QString SQL_CHECK_TABLES =
        "SELECT COUNT(*) FROM sqlite_master "
        "WHERE type = 'table' AND name <> 'sqlite_sequence';";

    const QStringList SQL_NEW_DATABASE_QUERIES = {
        "DROP TABLE IF EXISTS items;",
        // ----- //
        "CREATE TABLE IF NOT EXISTS items ("
        "  id INTEGER,"
        "  short_name TEXT NOT NULL,"
        "  long_name TEXT NOT NULL UNIQUE,"
        "  PRIMARY KEY(id AUTOINCREMENT));",
        // ----- //
        "DROP TABLE IF EXISTS properties;",
        // ----- //
        "CREATE TABLE IF NOT EXISTS properties ("
        "  item_id INTEGER,"
        "  property TEXT,"
        "  value TEXT,"
        "  CONSTRAINT 'PK' PRIMARY KEY(item_id, property));",
        // ----- //
        "DROP INDEX IF EXISTS property_name;",
        // ----- //
        "CREATE INDEX IF NOT EXISTS property_name ON properties (property ASC);",
        // ----- //
        "DROP INDEX IF EXISTS property_value;",
        // ----- //
        "CREATE INDEX IF NOT EXISTS property_value ON properties (value ASC);",
        // ----- //
        "DROP INDEX IF EXISTS item_short_name;",
        // ----- //
        "CREATE INDEX IF NOT EXISTS item_short_name ON items (short_name ASC);"
    };

    const QString SQL_GET_ITEM_PROPERTIES =
        "SELECT property, value FROM properties "
        "WHERE item_id = (SELECT id FROM items WHERE short_name = :short_name);";

    const QString SQL_GET_ITEM_PROPERTY =
        "SELECT value FROM properties "
        "WHERE property = :property "
        "  AND item_id = (SELECT id FROM items WHERE short_name = :short_name);";

    const QString SQL_UPDATE_ITEM_PROPERTY =
        "UPDATE properties SET value = :value "
        "WHERE property = :property "
        "  AND item_id = (SELECT id FROM items WHERE short_name = :short_name);";

    const QString SQL_SET_ITEM_NAME =
        "UPDATE items SET long_name = :long_name "
        "WHERE short_name = :short_name;";

    const QString SQL_ADD_ITEM_PROPERTY =
        "INSERT INTO properties (item_id, property, value) "
        "VALUES ((SELECT id FROM items WHERE short_name = :short_name), :property, :value);";

    const QString SQL_ADD_ITEM =
        "INSERT INTO items (short_name, long_name) "
        "VALUES (:short_name, :long_name);";

    const QString SQL_DELETE_ITEM_PROPERTY =
        "DELETE FROM properties "
        "WHERE property = :property "
        "  AND item_id = (SELECT id FROM items WHERE short_name = :short_name);";

    const QString SQL_DELETE_ITEM_PROPERTIES =
        "DELETE FROM properties "
        "WHERE item_id = (SELECT id FROM items WHERE short_name = :short_name);";

    const QString SQL_DELETE_ITEM =
        "DELETE FROM items "
        "WHERE short_name = :short_name;";

    const QString SQL_GET_ALL_ITEMS =
        "SELECT short_name, long_name FROM items "
        "ORDER BY short_name, long_name;";

    const QString SQL_SEARCH_ITEMS_BY_NAME =
        "SELECT short_name, long_name FROM items "
        "WHERE short_name LIKE :search_a OR long_name LIKE :search_b "
        "ORDER BY short_name, long_name;";

    const QString SQL_SEARCH_PROPERTIES_BY_NAME =
        "SELECT short_name, long_name FROM items "
        "WHERE id IN (SELECT DISTINCT item_id FROM properties WHERE property LIKE :property) "
        "ORDER BY short_name, long_name;";

    const QString SQL_SEARCH_PROPERTIES_BY_VALUE =
        "SELECT short_name, long_name FROM items "
        "WHERE id IN (SELECT DISTINCT item_id FROM properties WHERE value LIKE :value) "
        "ORDER BY short_name, long_name;";

    const QString SQL_GET_ALL_PROPERTIES =
        "SELECT DISTINCT property FROM properties ORDER BY property;";
};

#endif