# wat

Or, a mis-spelled "what".

`wat` is a small utility for storing information about things in an organized and searchable manner, with emphasis on being fast and easy to use on terminals. The application uses SQLite as data store and is built with Qt5. I originally developed this tool to keep track of IP addresses of my devices around the house, but it's really a super simple, yet general data storage application.

## Ideology

The idea behind `wat` is quite simple:

- Information is "attached" to a unique **item**
- Every item can have any number of distinctly-named **properties**
- Every property has a **value** of some sorts

So you can simply add an item and add properties to it.

## Example

To better illustrate the usage, a practical example should do the trick. Let's say we want to build a small address book:

```bash
# Let's add John
$ wat add john "John Doe"
$ wat john phone 12345
$ wat john addr "Street 12"
$ wat john car SmartyXL

# List everything we have about John
$ wat john
addr:   Street 12
car:    SmartyXL
phone:  12345

# Now let's add Jack
$ wat add jack "Jack Doe"
$ wat jack phone 54321
$ wat jack addr "Elsewhere 33"
$ wat jack laptop "Lappier 9000"
$ wat jack
addr:   Elsewhere 33
laptop: Lappier 9000
phone:  54321

# Who has a car?
$ wat -p car
john:   John Doe

# Who has a Lappier laptop?
$ wat -v Lappier
jack:   Jack Doe

# I need John's address
$ wat john addr
Street 12

# What kind of data do I have again?
$ wat --props
addr
car
laptop
phone
```

The database can and will only grow organically, as you enter data to it. It imposes no other limitations that what is written above. Okay, you can't use `add` or `del` as property names, as they are reserved for adding and deleting items and properties (although adding a property is done automatically).

## "Advanced" usage

If you're sneaky, you could turn this into a shell script collection...

```bash
$ wat code fixme "grep --color=never -RnE \"// (FIXME|TODO|CRITICAL)\" ."
$ eval $(wat code fixme)
application.cpp
./src/application.cpp:3:// FIXME This is a hack, but I'm lazy
```

...but that's probably a bad idea. You should just write the script or use aliases or something.

Just don't use it to save any passwords and such - the database is not access-controlled (it's SQLite, duh) nor encrypted (it's SQLite, duh).

Happy hacking!
